#!/bin/bash

MAX_TEST_ATTEMPTS="5"

runTest() {
  phantomjs out.js "$TEST_NUMBER"
  python3 pil.py
  aws sagemaker-runtime invoke-endpoint --endpoint-name ui-sanity-test --body fileb://out-240.png output.json > /dev/null
  local pass_output="$(cat output.json |jq |tail -n2 |head -n1 |xargs |sed "s,e,E,g")"
  if (( $(echo "$pass_output < 0.5" |bc -l) )); then
    TEST_RESULT="FAIL"
  else
    TEST_RESULT="PASS"
  fi
}


for TEST_NUMBER in {0..9}; do
  unset TEST_RESULT
  test_attempt="0"
  num_passes="0"
  while [[ $test_attempt -lt $MAX_TEST_ATTEMPTS ]]; do
    test_attempt=$(($test_attempt + 1))
    runTest
    if [ "$TEST_RESULT" == "PASS" ]; then
      num_passes=$(($num_passes + 1))
    fi
  done
  printf "Test ${TEST_NUMBER} passed ${num_passes} out of ${MAX_TEST_ATTEMPTS}\n"
done
