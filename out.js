var system = require('system');
var args = system.args;

var queryParams = "";
if (args.length === 2) {
  queryParams = "?test=" + args[1];
}

var page = require('webpage').create();
page.viewportSize = { width: 800, height: 600 };
page.clipRect = { top: 0, left: 0, width: 800, height: 800 };
page.open('http://localhost:8080' + queryParams, function() {
  page.render('out.png');
  phantom.exit();
});
